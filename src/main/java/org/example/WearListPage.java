package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WearListPage {

    @FindBy(xpath = "//input[@class=\"t-store__filter__input js-store-filter-search\"]")
    private WebElement searchForm;

    @FindBy(xpath = "//*[@class=\"t-store__search-icon js-store-filter-search-btn\"]")
    private WebElement searchButton;

    @FindBy(xpath = "//span[@class=\"js-store-filters-prodsnumber\"]")
    private WebElement productNumber;

    @FindBy(xpath = "//div[@class=\"js-store-prod-name js-product-name t-store__card__title t-name t-name_md\"]")
    private WebElement productName;

    @FindBy(xpath = "//div[@class=\"js-product-price js-store-prod-price-val t-store__card__price-value notranslate\"]")
    private WebElement productPrice;

    @FindBy(xpath = "//a[@href=\"https://homebrandofficial.ru/tproduct/544765431-930803998551-futbolka-oversize\"]")
    private WebElement tShirtOversizeRef;

    @FindBy(xpath = "//a[@class=\"t-store__prod-popup__btn t-btn t-btn_sm\"]")
    private WebElement addToCartButton;

    @FindBy(xpath = "//*[@class=\"t706__carticon t706__carticon_sm t706__carticon_showed\"]")
    private WebElement openCartButton;

    @FindBy(xpath = "//button[@class=\"t706__sidebar-continue t-btn\"]")
    private WebElement checkoutButton;

    @FindBy(xpath = "(//*[@name=\"tildaspec-phone-part[]\"])[2]")
    private WebElement phoneInputForm;

    @FindBy(xpath = "//div[@class=\"t-form__submit\"]/button[text()=\"ОФОРМИТЬ ЗАКАЗ\"]")
    private WebElement checkoutCartButton;

    @FindBy(xpath = "//p[@class=\"t-form__errorbox-item js-rule-error js-rule-error-phone\" and @style=\"display: block;\"]")
    private WebElement incorrectPhoneNumberErrorMessage;

    @FindBy(xpath = "//div[@class=\"t-input-error\" and @id=\"error_1496239478607\"]")
    private WebElement phoneInputFormIncorrectErrorMessage;

    private final WebDriver driver;

    public WearListPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WearListPage openPage() {
        driver.get("https://homebrandofficial.ru/wear");
        return this;
    }

    public WearListPage setTextForSearch(String pattern) {
        searchForm.sendKeys(pattern);
        return this;
    }

    public WearListPage clickSearchButton() {
        searchButton.click();
        return this;
    }

    public WearListPage clickOnTShirt() {
        tShirtOversizeRef.click();
        return this;
    }

    public WearListPage addToCart() {
        addToCartButton.click();
        return this;
    }

    public WearListPage openCart() {
        openCartButton.click();
        return this;
    }

    public WearListPage clickCheckoutButton() {
        checkoutButton.click();
        return this;
    }

    public WearListPage fillRandomPhoneNumberIntoCartPhoneNumberInputForm() {
        new WebDriverWait(driver, Duration.ofSeconds(20)).until(ExpectedConditions.elementToBeClickable(phoneInputForm))
                .click();
        phoneInputForm.sendKeys("0000000000");
        return this;
    }

    public WearListPage clickCheckoutCartButton() {
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(checkoutCartButton)).click();
        return this;
    }

    public String getProductNumber() {
        return productNumber.getText();
    }

    public String getProductName() {
        return productName.getText();
    }

    public String getProductPrice() {
        return productPrice.getText();
    }

    public String getIncorrectPhoneNumberErrorMessage() {
        return incorrectPhoneNumberErrorMessage.getText();
    }

    public String getPhoneInputFormIncorrectErrorMessage() {
        return phoneInputFormIncorrectErrorMessage.getText();
    }
}
