import org.assertj.core.api.SoftAssertions;
import org.example.WearListPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.time.Duration;

public class HomeBrandTest {

    private WebDriver driver;
    private SoftAssertions assertions;
    private WearListPage wearListPage;

    @Before
    public void onSetup() {
        driver = new EdgeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
        assertions = new SoftAssertions();
        wearListPage = new WearListPage(driver);
    }

    @After
    public void onEnd() {
        assertions.assertAll();
        driver.quit();
    }

    @Test
    public void testFirstScenario() {
        // 1. Открыть ссылку https://homebrandofficial.ru/wear
        // 2. В строку поиска ввести "Лонгслив White&Green"
        // 3. Нажать на иконку “Поиск”
        wearListPage.openPage()
                .setTextForSearch("Лонгслив White&Green")
                .clickSearchButton();

        // Проверить:
        // 4. Найден 1 товар
        String count = wearListPage.getProductNumber();
        String expectedCount = "1";
        assertions.assertThat(count).as("Неправильное количество товара")
                .isEqualToIgnoringCase(expectedCount);

        // 5. Название товара “Лонгслив White&Green”
        String productName = wearListPage.getProductName();
        String expectedProductName = "Лонгслив White&Green";
        assertions.assertThat(productName).as("Неправильное название товара")
                .isEqualToIgnoringCase(expectedProductName);

        // 6. Стоимость товара “2800”
        String productCost = wearListPage.getProductPrice();
        String expectedProductCost = "2 800";
        assertions.assertThat(productCost).as("Неправильная стоимость товара")
                .isEqualToIgnoringCase(expectedProductCost);
    }

    @Test
    public void testSecondScenario() {

        // 1. Открыть ссылку https://homebrandofficial.ru/wear
        // 2. Нажать на товар с названием “Футболка Оversize”
        // 3. Нажать на кнопку “Добавить в корзину”
        // 4. Нажать на иконку "Корзина"
        // 5. Нажать "Оформить заказ"
        // 6. Заполнить все поля рандомными данными (!телефон +7 (000) 000-00-00)
        // 7. Нажать кнопку “Оформить заказ”
        wearListPage.openPage()
                .clickOnTShirt()
                .addToCart()
                .openCart()
                .clickCheckoutButton()
                .fillRandomPhoneNumberIntoCartPhoneNumberInputForm()
                .clickCheckoutCartButton();

        // 8. Проверить, что отображается текст “Укажите, пожалуйста, корректный номер телефона”
        // около поля "Телефон" и внизу страницы
        String correctPhoneNumberError = "Укажите, пожалуйста, корректный номер телефона";
        String firstLabel = wearListPage.getPhoneInputFormIncorrectErrorMessage();
        String secondLabel = wearListPage.getIncorrectPhoneNumberErrorMessage();
        assertions.assertThat(firstLabel).as("Неправильное отображение ошибки у поля ввода телефона")
                .isEqualToIgnoringCase(correctPhoneNumberError);
        assertions.assertThat(secondLabel).as("Неправильное отображение ошибки внизу страницы")
                .isEqualToIgnoringCase(correctPhoneNumberError);
    }
}
